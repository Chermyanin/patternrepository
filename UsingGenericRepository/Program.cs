﻿using System;
using System.Linq;
using Autofac;
using BLToolkit.Data;
using BLToolkit.Data.DataProvider;
using UsingGenericRepository.Repository;

namespace UsingGenericRepository
{
	class Program
	{
		static void Main(string[] args)
		{
			try
			{
				var builder = new ContainerBuilder();
				//Установка для репозиториев AccessDataProvider
				builder.Register<Func<Type, DbManager>>(c => e =>
				{
					var fullName = e.FullName;

					if (string.IsNullOrEmpty(fullName))
						throw new NotSupportedException();

					if (fullName.Contains("DemoConnectionOne"))
						return new TestOneDataProvider<SqlDataProvider>();

					if (fullName.Contains("DemoConnectionSecond"))
						return new TestSecondDataProvider<AccessDataProvider>();

					throw new NotSupportedException();
				}).AsSelf();

				builder.RegisterGeneric(typeof(Repository<>))
					   .PropertiesAutowired()
					   .AsSelf();

				builder.RegisterType<AlcoProductRepository>().As<IAlcoProductRepository>();
				var container = builder.Build();
				var alcoRepository = container.Resolve<IAlcoProductRepository>();

				var unit = alcoRepository.GetAlcoProductByCode(2).First();
				Console.WriteLine("EntityName = {0}", unit.Name);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.ToString());
			}
			
			Console.ReadLine();
		}
	}
}
