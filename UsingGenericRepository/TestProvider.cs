﻿using System.Configuration;
using BLToolkit.Data;
using BLToolkit.Data.DataProvider;

namespace UsingGenericRepository
{
/// <summary>
/// Менеджер запросов к СУБД
/// Инкапсулирует соединение и низкоуровневую логику работы с СУБД
/// </summary>
public class TestOneDataProvider<TP> : DbManager
	where TP : DataProviderBase, new()
{
	/// <summary>
	/// Ключ для строки соединения провайдера по умолчанию
	/// </summary>
	public const string DefaultConfigurationString = "DemoConnectionOne";

	#region [ .ctor ]

	/// <summary>
	/// Регистрируем ODP провайдер
	/// </summary>
	static TestOneDataProvider()
	{
		AddDataProvider(DefaultConfigurationString, new TP());
		AddConnectionString(DefaultConfigurationString, ConnectionString);
	}

	#endregion

	#region Constructor(s)

	public TestOneDataProvider()
		: this(DefaultConfigurationString)
	{
	}

	public TestOneDataProvider(string configurationString)
		: base(configurationString)
	{
	}

	#endregion

	public static string ConnectionString
	{
		get
		{
			return ConfigurationManager.ConnectionStrings[DefaultConfigurationString].ConnectionString;
		}
	}
}

/// <summary>
/// Менеджер запросов к СУБД
/// Инкапсулирует соединение и низкоуровневую логику работы с СУБД
/// </summary>
public class TestSecondDataProvider<TP> : DbManager
	where TP : DataProviderBase, new()
{
	/// <summary>
	/// Ключ для строки соединения провайдера по умолчанию
	/// </summary>
	public const string DefaultConfigurationString = "DemoConnectionSecond";

	#region [ .ctor ]

	/// <summary>
	/// Регистрируем ODP провайдер
	/// </summary>
	static TestSecondDataProvider()
	{
		AddDataProvider(DefaultConfigurationString, new TP());
		AddConnectionString(DefaultConfigurationString, ConnectionString);
	}

	#endregion

	#region Constructor(s)

	public TestSecondDataProvider()
		: this(DefaultConfigurationString)
	{
	}

	public TestSecondDataProvider(string configurationString)
		: base(configurationString)
	{
	}

	#endregion

	public static string ConnectionString
	{
		get
		{
			return ConfigurationManager.ConnectionStrings[DefaultConfigurationString].ConnectionString;
		}
	}
}
}
