﻿using System.Configuration;
using BLToolkit.Data;
using BLToolkit.Data.DataProvider;

namespace UsingGenericRepository.Repository
{
	public class DataProvider<T> :  DbManager
        where T : DataProviderBase, new()
	{
		/// <summary>
		/// Ключ для строки соединения провайдера по умолчанию
		/// </summary>
		public const string DefaultConfigurationString = "MetaData";

		#region [ .ctor ]

		/// <summary>
		/// Регистрируем ODP провайдер
		/// </summary>
		static DataProvider()
		{
            AddDataProvider(DefaultConfigurationString, new T());
		    
            AddConnectionString(DefaultConfigurationString, ConfigurationManager.ConnectionStrings["MetaData"].ConnectionString);
		}

		#endregion

		#region Constructor(s)

		public DataProvider()
			: this(DefaultConfigurationString)
		{
		}

		public DataProvider(string configurationString)
			: base(configurationString)
		{
		}

		#endregion


	    public string ConnectionString 
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["MetaData"].ConnectionString;
            }
        }
	}
}
