﻿using System;
using System.Linq;
using System.Linq.Expressions;
using UsingGenericRepository.Model;

namespace UsingGenericRepository.Repository
{
	public interface IAlcoProductRepository
	{
		IQueryable<AlcoProductEntity> GetAlcoProductByCode(int? code);

		IQueryable<AlcoProductEntity> GetAlcoProductByName(string name);

		IQueryable<AlcoProductEntity> FindAll(Expression<Func<AlcoProductEntity, bool>> where = null);
	}
}
