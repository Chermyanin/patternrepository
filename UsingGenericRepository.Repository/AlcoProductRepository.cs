﻿using System;
using System.Linq;
using System.Linq.Expressions;
using UsingGenericRepository.Model;

namespace UsingGenericRepository.Repository
{
	public class AlcoProductRepository : Repository<AlcoProductEntity>, IAlcoProductRepository
	{
		public IQueryable<AlcoProductEntity> GetAlcoProductByCode(int? code)
		{
			return Select(x => x.Code == code);
		}

		public IQueryable<AlcoProductEntity> GetAlcoProductByName(string name)
		{
			return Select(x => x.Name == name);
		}

		public IQueryable<AlcoProductEntity> FindAll(Expression<Func<AlcoProductEntity, bool>> @where = null)
		{
			return Select(@where);
		}
	}
}
