﻿using System.Collections.Generic;

namespace UsingGenericRepository.Model
{
	public interface IEntity
	{
		long Id { get; set; }
		bool IsDirty { get; }
		bool IsNew { get; }

		Dictionary<string, object> Parameters { get; }

		IEntity CopyTo(IEntity entity);
		object Clone();
		object GetValue(string fieldName);
		void SetValue(string fieldName, object value);
	}
}
