﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using BLToolkit.DataAccess;
using BLToolkit.EditableObjects;
using BLToolkit.Mapping;
using BLToolkit.Reflection;

namespace UsingGenericRepository.Model
{
	/// <summary>
	/// Базовый контейнер для передачи в БД информации про сущность
	/// </summary>
	[Serializable]
	public abstract class EntityBase : EditableObject, IEntity
	{
		/// <summary>
		/// Свойство определяющее,обьект новый или нет 
		/// </summary>
		[MapIgnore]
		public bool IsNew
		{
			get
			{
				return Id <= 0;
			}
		}

		#region IEntity Members

		/// <summary>
		/// Идентификатор обьекта
		/// </summary>
		[PrimaryKey, NonUpdatable]
		public abstract long Id { get; set; }

		/// <summary>
		/// Параметры обьекта
		/// </summary>
		[MapIgnore]
		public virtual Dictionary<string, object> Parameters
		{
			get
			{
				var objMapper = Map.GetObjectMapper(GetType());

				var result = (from fieldName in objMapper.FieldNames
							  select new
							  {
								  FieldName = fieldName,
								  Value = objMapper.GetValue(this, fieldName)
							  })
							   .ToDictionary(o => o.FieldName, o => o.Value);

				return result;
			}
		}

		/// <summary>
		/// Извлечение значения из обьекта, по имени поля в бд
		/// </summary>
		/// <param name="fieldName"></param>
		/// <returns></returns>
		public object GetValue(string fieldName)
		{
			if (string.IsNullOrEmpty(fieldName))
				throw new ArgumentNullException();

			var objMapper = Map.GetObjectMapper(GetType());
			return objMapper.GetValue(this, fieldName);
		}

		/// <summary>
		/// Установка значения по имени поля в бд
		/// </summary>
		/// <param name="fieldName"></param>
		/// <param name="value"></param>
		public void SetValue(string fieldName, object value)
		{
			if (string.IsNullOrEmpty(fieldName))
				throw new ArgumentNullException();
			var objMapper = Map.GetObjectMapper(GetType());
			objMapper.SetValue(this, fieldName, value);
		}

		/// <summary>
		/// Копирование значений одного обьекта в другой
		/// </summary>
		/// <param name="entity">обьект в который необходимо скопировать</param>
		/// <returns></returns>
		public IEntity CopyTo(IEntity entity)
		{
			return CopyTo((EntityBase)entity);
		}

		/// <summary>
		/// Клонирование обьекта
		/// </summary>
		/// <returns></returns>
		object IEntity.Clone()
		{
			return Clone();
		}

		#endregion

		/// <summary>
		///  Копирование значений одного обьекта в другой
		/// </summary>
		/// <param name="obj">обьект в которій необходимо скопировать</param>
		/// <returns></returns>
		public virtual EntityBase CopyTo(EntityBase obj)
		{
			var properties = GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);

			foreach (var info in properties)
			{
				if (!info.CanRead)
					continue;

				var propDest = obj.GetType().GetProperty(info.Name);
				if (propDest == null || !propDest.CanWrite)
					continue;

				if (info.PropertyType != propDest.PropertyType)
					continue;

				var value = info.GetValue(this, null);
				propDest.SetValue(obj, value, null);
			}

			return obj;
		}

		/// <summary>
		/// Клонирование обьекта
		/// </summary>
		/// <returns></returns>
		public virtual EntityBase Clone()
		{
			var formatter = new BinaryFormatter();

			using (var stream = new MemoryStream())
			{
				formatter.Serialize(stream, this);
				//
				// Перемещаемся на начало, чтобы выполнить десериализацию
				//
				stream.Position = 0;
				return formatter.Deserialize(stream) as EntityBase;
			}
		}

		public static T Copy<T>(T entity)
			where T : EntityBase
		{
			return TypeAccessor<T>.Copy(entity, TypeAccessor<T>.CreateInstance());
		}
	}
}
