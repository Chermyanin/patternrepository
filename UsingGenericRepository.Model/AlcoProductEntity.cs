﻿using System;
using System.ComponentModel;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;
using BLToolkit.Validation;

namespace UsingGenericRepository.Model
{
	[Description("Спиртосодержащая продукция")]
	[TableName("AlcoProduct")]
	[Serializable]
	public abstract class AlcoProductEntity : EntityBase
	{
		[Required]
		[MapField("Code")]
		[Description("Код")]
		public abstract int? Code { get; set; }

		[MaxLength(255), Required]
		[MapField("Name")]
		[Description("Название")]
		public abstract string Name { get; set; }
	}
}
