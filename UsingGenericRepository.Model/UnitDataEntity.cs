﻿using System;
using System.ComponentModel;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;
using BLToolkit.Validation;

namespace UsingGenericRepository.Model
{
	[Description("Единицы измерения")]
	[TableName("Unit")]
	[Serializable]
	public abstract class UnitDataEntity : EntityBase
	{
		[MaxLength(3)]
		[MapField("Code")]
		[Description("Код")]
		public abstract string Code { get; set; }

		[MaxLength(100), Required]
		[MapField("ShortName")]
		[Description("Условное обозначение")]
		public abstract string ShortName { get; set; }

		[MaxLength(255), Required]
		[MapField("Name")]
		[Description("Название")]
		public abstract string Name { get; set; }
	}
}
